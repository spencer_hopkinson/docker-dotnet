FROM mcr.microsoft.com/dotnet/sdk:5.0
#Changed to use the SDK so publish tasks can run independently of code.

#This image is purely .NET core 5 runtime, no SDK 
#   - if using you'll need to publish on the build machine before copying published output to the image.
#FROM mcr.microsoft.com/dotnet/aspnet

WORKDIR /app
COPY ./src ./

RUN dotnet publish -c Release -o /var/www/dockerdotnet

WORKDIR /var/www/dockerdotnet

EXPOSE 80

# Can set env vars here, DockerRun.ps1 sets on container start
#ENV ASPNETCORE_ENVIRONMENT = "Development"
WORKDIR /var/www/dockerdotnet
ENTRYPOINT [ "dotnet", "docker-netcore.dll" ]