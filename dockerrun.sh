#!/bin/bash

clear

if [ $# -lt 2 ]
then
 echo "Less than 2 args supplied, exiting"
 exit 1
fi

if [ -z $1 ]
then
 echo "Please supply environment / container name in arg1"
 exit 1
fi

echo "Env:              $1"
echo "Local Port:       $2"
echo "$# args supplied"

docker run --name dockerdotnet-$1 -e "ASPNETCORE_ENVIRONMENT=$1" -e "ASPNETCORE_URLS=http://*:80" -d -p "$2:80" dockerdotnet