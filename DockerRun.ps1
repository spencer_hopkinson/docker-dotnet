param($env="Development",$localPort=5000)

<#
When running on linux, run PS in sudo mode before invoking this
i.e. 

sudo pwsh
./DockerRun.ps1
#>

Clear-Host

if($env -eq "")
{
    $env = "Production"
}

Write-Host Env param: $env -ForegroundColor Yellow
Write-Host Port param: $localPort -ForegroundColor Yellow

docker run --name dockerdotnet-$($env) -e "ASPNETCORE_ENVIRONMENT=$($env)" -e "ASPNETCORE_URLS=http://*:80" -d -p "$($localPort):80" dockerdotnet

Write-Host Sleeping a bit... -ForegroundColor Yellow
Start-Sleep -Seconds 2

$response = Invoke-WebRequest -uri http://localhost:$($localPort)/weatherforecast -UseBasicParsing

if($response.statusCode -eq 200)
{
    Write-Host Running OK!! -ForegroundColor Green    
}
else
{
    Write-Host FAILED! Status code $response.statusCode -ForegroundColor Red
}

